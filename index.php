<?php 

// declare(strict_types = 1);

include "includes/include-autoload.inc.php";


?>


<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<meta charset="utf-8">
  <title>Calc</title>
</head>
<body>
<form action="includes/calc.inc.php" class="container col-md-8" method="POST">
	<p>My oop calculator</p>
	<input type="number" name="num1" class="form-control" placeholder="First Number">
	<select name="oper" class="select" class="form-control form-control-sm">

		<option value="add">Addition</option>
		<option value="sub">Subtraction</option>
		<option value="div">Division</option>
		<option value="mul">Multiplication</option>
	</select>

	<input type="number" class="form-control" name="num2" placeholder="Second number">
	<button type="submit" class="btn btn-primary" name="submit">Calculate</button>
</form>




</body>
</html>