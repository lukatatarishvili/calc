<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width">
  <title>numbers base</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
  <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
  <style type="text/css">
    .hidden {
      display: none;
    }
    .search_history > span {
      padding: 3px 7px;
      border: 1px solid #e9e9e9;
      /*background: #ddd;*/
      background-color: #f3f3f3;
      border-radius: 3px;
      margin-right: 20px;
    }
    .usernum {
        height: calc(2.25rem + 2px);
        padding: .375rem .75rem;
        font-size: 1rem;
        font-weight: 400;
        line-height: 1.5;
        color: #495057;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid #ced4da;
        border-radius: .25rem;
        transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
    }

    }
    .search_history {
      width: 99%;
      /*overflow-x: scroll;*/
      overflow: hidden;
    }
    .GetLastSearchs_box {
      margin-top: 90px;
    }
    .search_actions_bar {
      margin-top: 120px;
    }
  </style>
</head>
<body>
    
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="/">ნომრების ბაზა, просто simple as that</a>
</nav>



<div class="container">
    <div class="row row2 clearfix search_actions_bar">
        <div class="col-xs-12 col-md-6">
            <form action="" method="get" id="numForm"> 
                <div class="form-group">
                    <input type="tel" name="number" class="usernum" placeholder="მიუთითეთ ნომერი">
                    <button type="submit" name="submit" class="btn btn-primary">ძებნა </button>
                </div>
            </form>
        </div>


          <div class="col-md-6">
              <div id="loaderDiv" class="loader hidden">
                <!--<img src="https://online.naec.ge/ueereg/DXR.axd?r=1_95-ormYb">-->
                    დაელოდეთ  ...
              </div>
              <h3 class="result_area"></h3>
          </div>
    </div>

</div>
  




                <script type="text/javascript">
                    $('#numForm').submit(function() {
                      var num = $('.usernum').val();
                      $.ajax({
                        url : 'Search.php?number=' + num,
                        type: 'GET',
                        beforeSend: function() {
                          $(".loader").removeClass('hidden');
                          $(".result_area").html("");
                        },
                        success: function(resp) { 
                            
                          $("#loaderDiv").addClass('hidden');
                          var data = JSON.parse(resp);
                          
                          if(data.res == "no") {
                              $('.result_area').html("ნომერი ვერ მოიძებნა");
                          } else if(data.res == "yes") {
                              $.each(data.items, function(m_i, m_item) {
                                  var export_data = '<span style=\"margin: 3px;\" class=\"badge badge-pill badge-dark\">' + m_item + '</span>';
                                  $('.result_area').append(export_data);
                              });
                          } else {
                              $('.result_area').html("რაღაც ძაან ნიტოა");
                          }
                          
                            // if(resp.res == "no") {
                            //     $('.result_area').html("ნომერი ვერ მოიძებნა");
                            // } else {
                            //     $("#loaderDiv").addClass('hidden');
                            //     var data = JSON.parse(resp);
                            //     $.each(data.items, function(m_i, m_item) {
                            //         var export_data = '<span style=\"margin: 3px;\" class=\"badge badge-pill badge-dark\">' + m_item + '</span>';
                            //         $('.result_area').append(export_data);
                            //     });
                            // }
                        },
                        error: function () {
                          $("#loaderDiv").addClass('hidden');
                          alert("დაფიქსირდა შეცდომა, შეამოწმეთ ინტერნეტთან კავშირი");
                        }
                      });
                      return false;
                    });
              </script>


    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-130173225-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
    
      gtag('config', 'UA-130173225-1');
    </script>


</body>
</html>